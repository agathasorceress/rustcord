use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
};

// shorter version of .to_string()
macro_rules!s( ( $e:expr ) => ( ($e).to_string() ) );

// Calculates hash of a type that implements Hash
pub fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}
