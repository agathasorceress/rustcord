#![allow(clippy::unreadable_literal)]
#![allow(clippy::cmp_owned)]

#[macro_use]
extern crate lazy_static;

use colored::*;
use rand::Rng;
use serenity::{
    async_trait,
    client::{
        bridge::gateway::{GatewayIntents, ShardId, ShardManager},
        Client,
    },
    framework::standard::{
        macros::{check, command, group, hook},
        Args, CommandOptions, CommandResult, DispatchError, Reason, StandardFramework,
    },
    model::{
        channel::{Message, ReactionType},
        gateway::Ready,
        id::UserId,
        user::OnlineStatus,
    },
    prelude::*,
};
use std::{env, process, sync::Arc};

#[macro_use]
mod utils;
use utils::*;

mod commands;
use commands::{
    brainfuck::*, define::*, embed::*, emote::*, help::*, lyrics::*, pinned::*, ship::*, spoile::*,
};

struct Handler;

struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        if let Some(shard) = ready.shard {
            println!(
                "INFO: {} is connected on shard {}/{}!\nuwu",
                ready.user.name, shard[0], shard[1]
            );

            use serenity::model::gateway::Activity;
            let activity = Activity::listening("catgirls nyaaing");
            let status = OnlineStatus::Online;

            ctx.set_presence(Some(activity), status).await;
        }
    }

    async fn message(&self, ctx: Context, message: Message) {
        let text = &message.content.to_lowercase();
        if text.contains("good")
            && (text.contains("discordinator") || text.contains("discordinyator"))
        {
            let _ = message.channel_id.say(&ctx.http, "nyaa~ 💞").await;
        }
    }
}

#[hook]
async fn dispatch_error(ctx: &Context, msg: &Message, error: DispatchError) {
    if let DispatchError::CheckFailed("Owner", Reason::Unknown) = error {
        // triggers if user is not owner
        let _ = msg.channel_id.say(&ctx.http, "nyo").await;
    } else if let DispatchError::Ratelimited(_) = error {
        // triggers if rate limited
        eprintln!(
            "{}",
            format!(
                "Rate limited in {} with message {}",
                s!(msg.channel_id).purple().bold(),
                msg.content.purple()
            )
        );
    }
}

#[hook]
async fn after(ctx: &Context, msg: &Message, command_name: &str, command_result: CommandResult) {
    // prints error in chat
    match command_result {
        Ok(()) => (),
        Err(why) => {
            let _ = msg
                .channel_id
                .send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e.title(format!("Error in **{}**", command_name))
                            .description(&why.to_string())
                            /*.thumbnail("https://i.imgur.com/VzOEz2E.png") oh no */
                            .colour(0xff6961)
                    })
                })
                .await;
            // prints error in console
            eprintln!(
                "{}",
                format!(
                    "Error in {}: {}",
                    command_name.purple(),
                    &why.to_string().red().bold()
                )
            );
        }
    }
}

#[group]
#[commands(
    init, ping, halt, servers, host, status, ship, bottom_rng, headpat, uwu, gayculator, sausage,
    help, embed, define, owo, info, echo, desc, pinned, brainfuck, pfp, lyrics, emote, spoiler
)]
struct General;

lazy_static! {
    static ref OWNERS: std::vec::Vec<serenity::model::id::UserId> =
        /*         Agatha's Id                  Julia's Id        */
        vec![UserId(254310746450690048), UserId(687740609703706630)];
}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| {
            c.with_whitespace(true)
                .owners(OWNERS.clone().into_iter().collect())
                .prefixes(vec!["owo!", "OwO!", "aga"])
                .no_dm_prefix(true)
                .case_insensitivity(true)
                .by_space(false)
        })
        .group(&GENERAL_GROUP)
        .on_dispatch_error(dispatch_error)
        .after(after);

    let mut client = Client::builder(&env::var("DISCORD_TOKEN").expect("Invalid token"))
        .event_handler(Handler)
        .framework(framework)
        .intents({
            let mut intents = GatewayIntents::all();
            intents.remove(GatewayIntents::GUILD_PRESENCES);
            intents
        })
        .await
        .expect("Error creating client");

    // Updates stored data, used for ping command
    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
    }

    if let Err(e) = client.start().await {
        eprintln!("An error occurred while running the client: {:?}", e);
    }
}

#[check]
#[name = "Owner"]
async fn owner_check(
    _: &Context, msg: &Message, _: &mut Args, _: &CommandOptions,
) -> Result<(), Reason> {
    if OWNERS.clone().contains(&msg.author.id) {
        Ok(())
    } else {
        Err(Reason::Unknown)
    }
}

#[check]
#[name = "Server"]
async fn server_check(
    _: &Context, msg: &Message, _: &mut Args, _: &CommandOptions,
) -> Result<(), Reason> {
    match msg.guild_id == Some(serenity::model::id::GuildId(687011389294116875)) {
        true => Ok(()),
        false => Err(Reason::Unknown),
    }
}

#[command]
async fn init(ctx: &Context, message: &Message) -> CommandResult {
    let responses = [
        "Discordinator9000 is gonna hug nya'll!",
        "Nyaa~!",
        "Hewwo uwu",
    ];
    let num = rand::thread_rng().gen_range(0..responses.len());
    let _ = message.channel_id.say(&ctx.http, responses[num]).await;

    Ok(())
}

#[command]
async fn ping(ctx: &Context, message: &Message) -> CommandResult {
    // I have no idea if this works but its 5æm and I need to sleep help
    let data = ctx.data.read().await;

    let shard_manager = match data.get::<ShardManagerContainer>() {
        Some(v) => v,
        None => return Err("There was a problem getting the shard manager!".into()),
    };

    let manager = shard_manager.lock().await;
    let runners = manager.runners.lock().await;

    let runner = match runners.get(&ShardId(ctx.shard_id)) {
        Some(v) => v,
        None => return Err("No shard found!".into()),
    };

    let ping = match runner.latency {
        Some(v) => v.as_millis(),
        None => return Err("Could not get latency!".into()),
    };

    let _ = message
        .channel_id
        .say(&ctx, format!("Pong! Latency: {}ms", ping))
        .await;

    Ok(())
}

#[command]
#[checks(Owner)]
async fn echo(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    let input: String = args.rest().trim().to_string();
    if args.is_empty() {
        return Err("Called without input".into());
    }

    let _ = message.channel_id.say(&ctx.http, input).await;

    Ok(())
}

#[command]
#[checks(Owner)]
async fn halt(ctx: &Context) -> CommandResult {
    // Workaround for discord not doing this automatically
    ctx.set_presence(None, OnlineStatus::Offline).await;

    use std::{thread, time};
    // Sleep for 1s
    thread::sleep(time::Duration::new(1, 0));

    process::exit(0);
}

// set bot's status to input text
#[command]
#[checks(Owner)]
async fn status(ctx: &Context, message: &Message, mut args: Args) -> CommandResult {
    use serenity::model::gateway::Activity;

    if args.is_empty() {
        return Err("Called without args!".into());
    }

    let mut input = args.single::<String>()?;
    input = input.trim().to_string();

    let activity;

    // if args contain 'listening', use the appropriate activity type
    if input.contains("listening") {
        activity = Activity::listening(args.rest().trim());
    // reset status
    } else if input.contains("reset") {
        activity = Activity::listening("catgirls nyaaing");
    // otherwise default to playing
    } else {
        args.restore();
        activity = Activity::playing(args.rest().trim());
    };

    let status = OnlineStatus::Online;
    ctx.set_presence(Some(activity), status).await;
    let _ = message
        .react(&ctx.http, ReactionType::Unicode("💜".into()))
        .await;

    Ok(())
}

#[command]
#[checks(Owner)]
async fn servers(ctx: &Context, message: &Message) -> CommandResult {
    let mut list = String::new();
    let cache = &ctx.cache;
    for (index, guild) in cache.guilds().await.iter().enumerate() {
        let name = guild
            .name(&ctx.cache)
            .await
            .unwrap_or_else(|| s!("unknown"));
        list += &format!("{}: {}\n", index, name);
    }
    let _ = message
        .channel_id
        // Add zero width space to all mentions
        .say(&ctx.http, list.replace("@", "@\u{200B}"))
        .await;

    Ok(())
}

#[command]
#[checks(Owner)]
async fn host(ctx: &Context, message: &Message) -> CommandResult {
    let _ = message
        .channel_id
        .say(
            &ctx.http,
            format!(
                "OS: {os}; {release}\nHost: {host}\nCPU: {cpu}MHz",
                os = sys_info::os_type()?,
                host = sys_info::hostname()?,
                release = sys_info::linux_os_release()?
                    .pretty_name
                    .unwrap_or_else(|| s!("Unknown")),
                cpu = sys_info::cpu_speed()?
            ),
        )
        .await;

    Ok(())
}

// generate a random number using a keysmash as seed
#[command]
async fn bottom_rng(ctx: &Context, message: &Message, mut args: Args) -> CommandResult {
    use rand::{rngs::StdRng, SeedableRng};

    // get N last messages, otherwise 10
    let num = args.single::<u64>().unwrap_or(10);
    let messages = message
        .channel_id
        .messages(&ctx.http, |get| get.before(message.id).limit(num))
        .await;
    if let Err(e) = messages {
        return Err(format!("Error: {}", e).into());
    } else {
        let mut messages = messages?;
        // remove all messages by other users
        messages.retain(|v| v.author != message.mentions[0]);
        let mut input = String::new();
        for msg in messages {
            input += &format!("{} ", msg.content);
        }
        let result: u64 = StdRng::seed_from_u64(calculate_hash(&input)).gen_range(0..100);
        let _ = message
            .channel_id
            .send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e.title("Bottom RNG")
                        .description(format!("Result: {}", result))
                        .color(0x800869)
                })
            })
            .await;
    }

    Ok(())
}

#[command]
#[aliases("pat")]
async fn headpat(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    let args = args.rest().trim();

    if args.is_empty() {
        return Err("Please specify a username!".into());
    }

    // Get username from first mention, otherwise use input text
    let name = match message.mentions.len() {
        0 => args,
        _ => message.mentions[0].name.as_str(),
    };

    if let Err(e) = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title(format!("Sending headpats to **{}**...", name))
                .image(
                    "https://i.pinimg.com/originals/83/1a/90/831a903eab6d827dcfd298b9e3196e30.jpg",
                )
                .description("[Source](https://www.pinterest.com/pin/377809856242075277/)")
            })
        })
        .await
    {
        let _ = message.channel_id.say(&ctx.http, format!("{:?}", e)).await;
    };

    Ok(())
}

// send a random uwu image
#[command]
async fn uwu(ctx: &Context, message: &Message) -> CommandResult {
    let images = [
        "https://i.redditmedia.com/qDD9W7NJqTAk31y061TuRW9R8qOcCuEmmCWyOsUEavE.png?fit=crop&crop=faces%2Centropy&arh=2&w=640&s=ebdd3f1970b4fe70ccd24a1958e7fc32",
        "https://www.shitpostbot.com/img/sourceimages/smash-that-mfuckn-uwu-button-57b5aa1de9fe4.jpeg",
        "https://www.shitpostbot.com/img/sourceimages/fallout-nv-owo-57e586ae15322.jpeg",
        "https://i.redditmedia.com/-JaK9YW7mPz2S2xBJmXvW4fZ58uGMa4l6GIgYt3dqZg.jpg?fit=crop&crop=faces%2Centropy&arh=2&w=640&s=ebab29a577346b4d18ec914538b69bb4",
        "https://preview.redd.it/ie48xuwurzt41.jpg?width=640&crop=smart&auto=webp&s=c4a27d5ed086430cd29530a3d3c8e846cad867d5",
        "https://i.redd.it/9aw0yzprztq41.jpg",
        "https://i.redd.it/gxjx9jb01ef41.jpg",
        "https://i.redd.it/n0liugufmks41.jpg",
        "https://preview.redd.it/fcfrmarhj9s41.jpg?width=640&crop=smart&auto=webp&s=5a4ff9a471dca7cad61b8e56bc65876ef083304a",
        "https://i.redd.it/ifwsmbme48q41.jpg"
    ];
    let num = rand::thread_rng().gen_range(0..images.len());
    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.image(images[num]);
                e.footer(|f| f.text(format!("Source: {}", images[num])));

                e
            })
        })
        .await;

    Ok(())
}

#[command]
async fn gayculator(ctx: &Context, message: &Message, mut args: Args) -> CommandResult {
    let number_32: i32 = args.single::<i32>().unwrap_or(1);
    let result = if number_32 % 2 == 0 {
        "much straight"
    } else {
        "large gay"
    };
    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Gayness level:")
                    .description(result)
                    .color(0xffd1dc)
            })
        })
        .await;

    Ok(())
}

#[command]
#[aliases("sosig")]
async fn sausage(ctx: &Context, message: &Message) -> CommandResult {
    let _ = message.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
		.title("Dongle!")
		.image("https://cdn.discordapp.com/attachments/689023662489468966/712283397015470120/26029881886330_4.gif")
        })
    }).await;
    Ok(())
}

#[command]
async fn info(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    if !args.is_empty() {
        return Err("Called with args!".into());
    }

    let num = ctx.cache.guilds().await.len();
    // get developer's username
    let aganame = OWNERS.clone()[0].to_user(ctx.http.clone()).await?.tag();
    let _ = message.channel_id.send_message(&ctx.http, |m| m
		.embed(|e| e
			.title("Discordinator9000's info:")
			.field("Author:", format!("{} / Agatha", aganame), false)
			.field("Server count:", num  , false)
			.field("Invite:", "[Invite link](https://discordapp.com/api/oauth2/authorize?client_id=470350233419907129&permissions=2048&scope=bot)", true )
            .field("source:", "[Gitlab](https://gitlab.com/agathasorceress/rustcord)", true)
			.footer(|f| f
			.text("Written in Rust using Serenity, OwOify and a few other libraries"))
            .thumbnail("https://cdn.discordapp.com/attachments/687011390434967621/704118007563157544/discordinator.png")
			.color(0xffd1dc)
			)).await;
    Ok(())
}

#[command]
async fn pfp(ctx: &Context, message: &Message) -> CommandResult {
    // Get username from first mention, otherwise use current username
    let user = match message.mentions.len() {
        0 => &message.author,
        _ => &message.mentions[0],
    };

    let pfp = match user.avatar_url() {
        Some(v) => v,
        None => return Err("The user does not have an avatar".into()),
    };

    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title(format!("{}'s profile picture", user.name))
                    .image(pfp)
                    .color(0xffd1dc)
            })
        })
        .await;

    Ok(())
}

// Text owoification
#[command]
async fn owo(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    use owoify::OwOifiable;

    let lastmsg = match message
        .channel_id
        .messages(&ctx.http, |get| get.before(message.id).limit(1))
        .await
    {
        Ok(v) => v,
        Err(_) => return Err("Could not get last message!".into()),
    };

    let lastmsg = &lastmsg[0].content;

    let input: String = match args.is_empty() {
        true => s!(lastmsg),
        false => args.rest().trim().to_string(),
    };
    let _ = message.channel_id.say(&ctx.http, input.owoify()).await;

    Ok(())
}

// Prints channel topic
#[command]
#[only_in(guilds)]
#[aliases("description", "topic")]
async fn desc(ctx: &Context, message: &Message) -> CommandResult {
    let channel = match message.channel(&ctx).await {
        Some(ch) => ch,
        None => {
            return Err("Could not get channel!".into());
        }
    };
    let channel = match channel.guild() {
        Some(g) => g,
        None => {
            return Err("Could not get guild channel!".into());
        }
    };

    let topic = if channel.topic.clone().unwrap() != "" {
        channel.topic.clone().unwrap()
    } else {
        String::from("No channel topic found")
    };

    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Channel's topic:")
                    .description(topic)
                    .color(0xffd1dc)
            })
        })
        .await;

    Ok(())
}
