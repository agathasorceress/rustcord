use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::*,
};

// Urban Dictionary lookup
#[command]
#[aliases("what's this")]
async fn define(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    let text: String = args.rest().trim().to_string();
    let defs = &urbandict::get_definitions(&text);
    if !args.is_empty() {
        match defs {
            Err(_e) => {
                return Err("Invalid query >w<".into());
            }
            Ok(v) => {
                if !v.is_empty() {
                    let def = &v[0];
                    let _ = message
                        .channel_id
                        .send_message(&ctx.http, |m| {
                            m.embed(|e| {
                                e.title(format!("Query: {}, Author: {}", text, def.author))
                                    .field(
                                        "Definition: ",
                                        def.definition.replace(|c| c == '[' || c == ']', ""),
                                        false,
                                    )
                                    .color(0xffd1dc)
                            })
                        })
                        .await;
                } else {
                    return Err("No results!".into());
                }
            }
        }
    }

    Ok(())
}
