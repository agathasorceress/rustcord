use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::*,
};

// brainfuck interpreter
#[command]
#[aliases("bf", "brainfrick")]
async fn brainfuck(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    use brainfrick::Brainfuck;

    let input = match args.rest().trim() {
        "" => {
            return Err("Called without input!".into());
        }
        v => v,
    };
    let output = Brainfuck::execute(input);

    match output {
        Ok(v) => {
            let _ = message
                .channel_id
                .send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e.title("Brainfuck interpreter")
                            .description(format!(
                                "Input\n```brainfuck\n{}\n```\nOutput:\n```{}\n```",
                                input, v
                            ))
                            .author(|a| {
                                a.name(&message.author.name)
                                    .icon_url(message.author.avatar_url().unwrap())
                            })
                            .colour(0xffd1dc)
                    })
                })
                .await;
        }
        Err(err) => {
            let _ = message
                .channel_id
                .send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e.title("Brainfuck interpreter")
                            .description(format!(
                                "Error at:\n```\n{}:{}\n```",
                                err.line(),
                                err.col()
                            ))
                            .author(|a| {
                                a.name(&message.author.name)
                                    .icon_url(message.author.avatar_url().unwrap())
                            })
                            .colour(0xff6961)
                    })
                })
                .await;
        }
    }

    Ok(())
}
