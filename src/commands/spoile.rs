use serenity::{
    builder::CreateMessage,
    framework::standard::{macros::command, Args, CommandResult},
    http::AttachmentType,
    model::channel::Message,
    prelude::*,
    utils::MessageBuilder,
};

#[command]
async fn spoiler(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    // check if the message has any attachments
    let attachments = match message.attachments.is_empty() {
        true => {
            return Err("No images were attached!".into());
        }
        false => &message.attachments,
    };

    // get the author's nick in the server, otherwise default to global username later
    let username = message
        .author
        .nick_in(&ctx.http, &message.guild_id.ok_or("No guild id found")?)
        .await;

    let mut output = MessageBuilder::new();

    &output
        .push("Sent by ")
        .push_bold_line_safe(username.unwrap_or(message.author.name.clone()));

    // if message contains text, add it
    match args.rest().trim() {
        "" => {}
        v => {
            &output.push(v);
        }
    }

    let output_message = &mut CreateMessage::default();
    output_message.content(output.build());

    for a in attachments {
        // download each attachment
        let content = match a.download().await {
            Ok(content) => content,
            Err(_) => return Err("Error downloading attachment".into()),
        };

        let content: &[u8] = content.as_slice();

        // attach them back to the new message with an edited filename
        output_message.add_file(AttachmentType::Bytes {
            data: content.to_owned().into(),
            filename: format!("SPOILER_{}", a.filename),
        });
    }

    // delete the original message
    let _ = message.delete(&ctx.http).await;

    let _ = message
        .channel_id
        .send_message(&ctx.http, |_| output_message)
        .await;

    Ok(())
}
