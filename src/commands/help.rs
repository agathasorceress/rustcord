use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::channel::Message,
    prelude::*,
};

#[command]
async fn help(ctx: &Context, message: &Message) -> CommandResult {
    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Availble commands:")
                    .description("All commands are case-insensitive")
                    .fields(vec![
                        ("owo!init", "Introduce me", true),
                        ("owo!ping", "Pong", true),
                        ("owo!sausage", "Sosig", true),
                        ("owo!help", "Help the fellow humanz!", true),
                        ("owo!info", "Show information about me!", true),
                        (
                            "owo!what's this ``word``",
                            "Find a definition of word",
                            true,
                        ),
                        (
                            "owo!spoiler ``<text>`` ``[image(s)]``",
                            "Re-sends a message with attachments spoilered. Text is optional",
                            true,
                        ),
                        (
                            "owo!embed ``[args]`` *OR* help",
                            "Create an embed from a Toml object",
                            true,
                        ),
                        ("owo!desc", "Display channel's topic", true),
                        (
                            "owo!pinned ``num`` ``<channel>``",
                            "Display channel's Nth pinned message. Channel name is optional",
                            true,
                        ),
                        ("owo!pfp ``@username``", "Post user's profile picture", true),
                        ("owo!brainfuck ``input``", "Execute input code", true),
                        ("owo!ship ``[names]``", "*Shipping intensifies*", true),
                        ("owo!headpat ``name``", "Headpat someone", true),
                        ("owo!owo ``text``", "owoify input text", true),
                        ("owo!lyrics ``name``", "Get song lyrics", true),
                        (
                            "owo!e ``emote``",
                            "Get a bigger version of a custom emote",
                            true,
                        ),
                        ("\u{200B}", "**Admin commands:**", false),
                        ("owo!halt", "Kill the bot process", true),
                        ("owo!status ``[args]``", "Sets the bot status", true),
                        ("owo!servers", "List the servers I'm in", true),
                        ("owo!host", "Display host info", true),
                    ])
                    .color(0xffd1dc)
            })
        })
        .await;

    Ok(())
}
