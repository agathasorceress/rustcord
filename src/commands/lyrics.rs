use percent_encoding::{percent_encode, NON_ALPHANUMERIC};
use serde::Deserialize;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::*,
};
use std::env;

#[derive(Deserialize)]
struct Response {
    result: Content,
    artist: String,
    song: String,
}

#[derive(Deserialize)]
struct Content {
    lyrics: String,
}

#[command]
async fn lyrics(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    // TODO: use https://orion.apiseeds.com/api/music/lyric/:artist/:track instead
    let mut url = String::from("https://mourits.xyz:2096/?q=");
    // check if input is not empty
    let input = match args.rest().trim() {
        "" => {
            return Err("Called without input!".into());
        }
        v => v,
    };
    // encode into url
    url += &s!(percent_encode(input.as_bytes(), NON_ALPHANUMERIC));

    let request = match reqwest::get(&url).await {
        Ok(v) => v,
        Err(e) => return Err(e.into()),
    };

    let resp: Response = match request.json().await {
        Ok(v) => v,
        Err(_) => return Err("Could not find lyrics".into()),
    };
    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title(format!("{} by {}", resp.song, resp.artist))
                    .description(resp.result.lyrics)
                    .colour(0xffd1dc)
            })
        })
        .await;

    Ok(())
}
