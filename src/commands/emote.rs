use regex::Regex;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::*,
    utils::parse_emoji,
};

#[command]
#[aliases("e")]
async fn emote(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    let input = match args.rest().trim() {
        "" => {
            return Err("Called without input!".into());
        }
        v => v,
    };

    let re = Regex::new(r"<a{0,1}:\w+:\d+>").unwrap();
    let emojis = match re.captures(input) {
        None => {
            return Err("No custom emojis found in the message!".into());
        }
        Some(v) => v,
    };

    let url = parse_emoji(&emojis[0]).unwrap().url();
    let _ = message.channel_id.say(&ctx.http, url).await;

    Ok(())
}
