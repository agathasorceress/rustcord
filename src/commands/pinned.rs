use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::{channel::Message, id::ChannelId},
    prelude::*,
};

// Prints Nth pinned message
#[command]
#[only_in(guilds)]
async fn pinned(ctx: &Context, message: &Message, mut args: Args) -> CommandResult {
    // defaults to latest pinned message if no args are provided
    let mut idx = args.single::<usize>().unwrap_or(1);
    // Makes pinned messages 1-indexed
    if idx != 0 {
        idx -= 1;
    }

    let target_channel = match args.single::<ChannelId>() {
        Ok(v) => v,
        Err(_) => message.channel_id,
    };
    let pinned = match target_channel.pins(&ctx.http).await {
        Ok(v) => v,
        Err(e) => {
            return Err(format!("Could not get pinned messages! Error: {}", e).into());
        }
    };
    if pinned.is_empty() {
        return Err("No pinned messages found!".into());
    }
    if idx > pinned.len() - 1 {
        return Err("Index out of bounds!".into());
    }

    let guild_id = match message.guild_id {
        Some(id) => id,
        None => return Err("Could not find Guild Id".into()),
    };

    let msg_link = format!(
        "https://discord.com/channels/{}/{}/{}",
        guild_id, &pinned[idx].channel_id, &pinned[idx].id
    );

    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title(format!("Pinned message #{}", idx + 1))
                    .description(&format!(
                        "{}\n[**🔗 link**]({})",
                        &pinned[idx].content, msg_link
                    ))
                    .timestamp(&pinned[idx].timestamp);
                e.author(|a| {
                    a.name(&pinned[idx].author.name)
                        .icon_url(&pinned[idx].author.avatar_url().unwrap())
                });
                e.colour(0xffd1dc);
                if !&pinned[idx].attachments.is_empty() {
                    e.image(&pinned[idx].attachments[0].url);
                }

                e
            })
        })
        .await;

    Ok(())
}
