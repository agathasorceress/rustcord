use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::*,
};
#[command]
async fn embed(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    use serde::Deserialize;
    use serenity::utils::Colour;
    use std::{fs, io::prelude::*};

    #[derive(Deserialize)]
    struct EmbedProperties {
        author: Option<(String, String)>,
        colour: Option<String>,
        description: Option<String>,
        fields: Option<Vec<(String, String, bool)>>,
        footer: Option<(String, String)>,
        image: Option<String>,
        thumbnail: Option<String>,
        timestamp: Option<String>,
        title: Option<String>,
        url: Option<String>,
    }

    // print documentation from src/embed-docs.txt
    if s!(&args.rest().trim()) == "help" {
        let mut file = fs::File::open("./src/commands/embed-docs.txt")?;
        let mut help_string = String::new();
        file.read_to_string(&mut help_string)?;

        let _ = message.channel_id.say(&ctx.http, help_string).await;

        return Ok(());
    }

    let input_embed: EmbedProperties = match toml::from_str(&args.rest().trim()) {
        Ok(v) => v,
        Err(e) => {
            return Err(format!("Deserialization error: {:?}", e).into());
        }
    };

    let _ = message
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                // Set embed author unless empty
                if input_embed.author.is_some() {
                    let auth = input_embed.author.unwrap();
                    e.author(|a| {
                        //assuming first array element is name and second is icon url
                        a.name(auth.0);
                        a.icon_url(auth.1);

                        a
                    });
                }

                // Set embed colour unless empty
                if input_embed.colour.is_some() {
                    e.color(Colour::new(
                        u32::from_str_radix(&input_embed.colour.unwrap(), 16)
                            .ok()
                            .unwrap_or(0x000000),
                    ));
                }

                // Set embed description unless empty
                if input_embed.description.is_some() {
                    e.description(input_embed.description.unwrap());
                }
                // Set embed fields unless empty
                if input_embed.fields.is_some() {
                    e.fields(input_embed.fields.unwrap());
                }

                // Set embed footer unless empty
                if input_embed.footer.is_some() {
                    let foot = input_embed.footer.unwrap();
                    e.footer(|f| {
                        //assuming first array element is name and second is icon url
                        f.text(foot.0);
                        f.icon_url(foot.1);

                        f
                    });
                }

                if input_embed.image.is_some() {
                    e.image(input_embed.image.unwrap());
                }

                if input_embed.thumbnail.is_some() {
                    e.thumbnail(input_embed.thumbnail.unwrap());
                }

                if input_embed.timestamp.is_some() {
                    e.timestamp(input_embed.timestamp.unwrap());
                }

                if input_embed.title.is_some() {
                    e.title(input_embed.title.unwrap());
                }

                if input_embed.url.is_some() {
                    e.url(input_embed.url.unwrap());
                }

                e
            });
            m
        })
        .await;

    let _ = message
        .channel_id
        .say(&ctx.http, format!("Embed requested by: {}", message.author))
        .await;

    Ok(())
}
