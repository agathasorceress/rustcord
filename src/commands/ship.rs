use crate::utils::calculate_hash;
use rand::Rng;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::*,
};

#[command]
async fn ship(ctx: &Context, message: &Message, args: Args) -> CommandResult {
    use rand::{rngs::StdRng, SeedableRng};

    // Get input names
    let names: String = args.rest().trim().to_string();
    // Calculate compatibility based on hash
    let compat: u64 = StdRng::seed_from_u64(calculate_hash(&names)).gen_range(50..100);

    // Initialize a bar to display compatibility percentage
    let mut compbar = String::from("----------");
    compbar.insert_str((compat / 10) as usize, ":purple_heart:");

    // Convert names to a Vec<String>
    let names = names
        .split_whitespace()
        .map(|x| x.to_owned())
        .collect::<Vec<String>>();

    // Concatenate names together
    let shipname: Result<String, String> = match names.len() {
        0 => Err(s!("Invalid input!")),
        1 => Ok(names[0].clone()),
        _ => {
            let mut first_halves = String::new();
            for name in &names[0..names.len() - 1] {
                first_halves += &name[0..name.len() / 2];
            }
            let first_halves = first_halves.as_str();
            let last_half = &names[names.len() - 1][(names.len() / 2) + 1..];

            Ok(format!("{}{}", first_halves, last_half))
        }
    };

    if let Err(e) = shipname {
        let _ = message.channel_id.say(&ctx.http, e).await;
    } else {
        let _ = message
            .channel_id
            .send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e.title(format!("Original names: {}", args.rest().trim()))
                        .description(format!(
                            "Ship name:\n**{}**\nCompatibility: **{}%**\n{}",
                            shipname.unwrap(),
                            compat,
                            compbar
                        ))
                        .color(0xffd1dc)
                })
            })
            .await;
    }

    Ok(())
}
